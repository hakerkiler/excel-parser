<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTownPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('town_people', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nr_from_doc_upload');
            $table->string('name')->index();
            $table->date('date_of_birth');
            $table->string('serials');
            $table->string('IDNP');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('town_people');
    }
}
