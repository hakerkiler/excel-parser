<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeoplePropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people_property', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('people_id');
            $table->string('nr_property');
            $table->string('volume');
            $table->string('cantitatea');
            $table->string('type_lot');
            $table->string('comment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('people_property');
    }
}
