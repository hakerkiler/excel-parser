<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TownPeople extends Model
{
    protected $table = 'town_people';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nr_from_doc_upload',
        'name',
        'date_of_birth',
        'serials',
        'IDNP',
    ];

}
