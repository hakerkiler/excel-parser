<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>
        <link rel="stylesheet" href="/css/app.css">
        <link rel="stylesheet" href="/css/demo.css">
        <link href="http://datatables.yajrabox.com/css/datatables.bootstrap.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-condensed dataTable" id="users-table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Id</th>
                                <th>Nr From Doc Upload</th>
                                <th>Name</th>
                                <th>Date Of Birth</th>
                                <th>Serials</th>
                                <th>IDNP</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                            </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>
        <script id="details-template" type="text/x-handlebars-template">
            <div class="label label-info">User @{{name}}'s Posts</div>
            <table class="table details-table" id="posts-@{{id}}">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>nr_property</th>
                    <th>volume</th>
                    <th>cantitatea</th>
                    <th>type_lot</th>
                    <th>comment</th>
                </tr>
                </thead>
            </table>
        </script>
        <!-- jQuery -->
        <script src="//code.jquery.com/jquery.js"></script>
        <!-- DataTables -->
        <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
        <!-- Bootstrap JavaScript -->
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <!-- App scripts -->
    <script src="/js/handlebars-v4.0.5.js"></script>
    <script>
        var template = Handlebars.compile($("#details-template").html());
        var table = $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route('home.getUserData') }}',
                method: 'POST'
            },
            columns: [
                {
                    "className":      'details-control',
                    "orderable":      false,
                    "data":           null,
                    "defaultContent": ''
                },
                {data: 'id', name: 'id'},
                {data: 'nr_from_doc_upload', name: 'nr_from_doc_upload'},
                {data: 'name', name: 'name'},
                {data: 'date_of_birth', name: 'date_of_birth'},
                {data: 'serials', name: 'serials'},
                {data: 'IDNP', name: 'IDNP'},
                {data: 'created_at', name: 'created_at'},
                {data: 'updated_at', name: 'updated_at'}
            ],
            order: [[1, 'asc']]
        });

        // Add event listener for opening and closing details
        $('#users-table tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var tableId = 'posts-' + row.data().id;

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(template(row.data())).show();
                initTable(tableId, row.data());
                tr.addClass('shown');
                tr.next().find('td').addClass('no-padding bg-gray');
            }
        });

        function initTable(tableId, data) {
            $('#' + tableId).DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: data.details_url,
                    method: 'POST'
                },
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'nr_property', name: 'nr_property' },
                    { data: 'volume', name: 'volume' },
                    { data: 'cantitatea', name: 'cantitatea' },
                    { data: 'type_lot', name: 'type_lot' },
                    { data: 'comment', name: 'comment' },
                ]
            })
        }
    </script>
    </body>
</html>
