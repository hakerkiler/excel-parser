<?php

namespace App\Http\Controllers;

use App\Models\PeopleProperty;
use App\Models\TownPeople;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Datatables;

class HomeController extends Controller
{
    /**
     * @param Request $request
     */
    function upload_file(Request $request){

        Excel::load('upload/copie.xls', function($reader) {

            $reader->noHeading();
            $reader->all();
            //dd($reader->all());
            $user_id = 0;
            foreach($reader->all() as $value){
                //dd(strpos($value[0], 'Nr'));
                if(strpos($value[0], 'Nr') === false){
                    if(is_float($value[0])){
                        $user = new TownPeople();
                        $user->nr_from_doc_upload = $value[0];
                        $info_user = explode(',', $value[2]);
                        $user->name = $info_user[0];

                        if(count($info_user) > 2){
                            $info_user[1] = str_replace(array('(', ')'), '', trim($info_user[1]));
                            $ida = Carbon::createFromFormat('d.m.Y', $info_user[1])->toDateString();

                            $user->date_of_birth = $ida;
                            $user->serials = trim($info_user[2]);
                        } else {
                            $info_user_2 = explode(' ', str_replace('  ', ' ', trim($info_user[1])));
                            $info_user[1] = str_replace(array('(', ')'), '', trim($info_user_2[0]));
                            $ida = Carbon::createFromFormat('d.m.Y', $info_user[1])->toDateString();

                            $user->date_of_birth = $ida;
                            $user->serials = trim($info_user_2[1]);
                        }
                        if($value[10] == null){
                            $user->IDNP = 'Not set';
                        } else {
                            $user->IDNP = $value[10];
                        }
                        $user->save();
                        $user_id = $user->id;

                    }
                    if($value[0] == null){
                        $property =  new PeopleProperty();
                        $property->people_id = $user_id;
                        $property->nr_property = $value[2];
                        $property_info = explode('--', trim($value[3]));

                        $property->volume = str_replace(array('(', ')'), '', trim($property_info[1]));
                        $property->cantitatea = trim($property_info[2]);
                        $property->type_lot = trim($property_info[3]);
                        $property->comment = trim($property_info[4]);

                        $property->save();
                    }

                }
            }
        });

    }
    public function getMaster()
    {
        return view('welcome');
    }

    public function getMasterData()
    {
        $users = TownPeople::select();

        return Datatables::of($users)
            ->addColumn('details_url', function($user) {
                return url('user/details/' . $user->id);
            })
            ->make(true);
    }

    public function getDetailsData($id)
    {
        $posts = PeopleProperty::where('people_id', $id)->get();

        return Datatables::of($posts)->make(true);
    }
}
