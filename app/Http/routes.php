<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/upload/file', [
    'uses' => 'HomeController@upload_file',
    'as' => 'home.upload_file'
]);

Route::get('/', [
    'uses' => 'HomeController@getMaster',
    'as' => 'home.getUser'
]);
Route::post('/user', [
    'uses' => 'HomeController@getMasterData',
    'as' => 'home.getUserData'
]);

Route::post('/user/details/{id}', [
    'uses' => 'HomeController@getDetailsData',
    'as' => 'home.getDetailsData'
]);
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
