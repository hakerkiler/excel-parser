<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PeopleProperty extends Model
{
    protected $table = 'people_property';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'people_id',
        'nr_property',
        'volume',
        'cantitatea',
        'type_lot',
        'comment',
    ];

}
